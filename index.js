
// Initialization
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pickachu", "Charizard","Squirtle","Balbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"],
	},
	talk: function(){
					console.log("Pikachu, I choose you!")
				}

}
console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of bracket notation:")
console.log(trainer["pokemon"]);

trainer.talk();


// Constructor
function Pokemon(name,level) {
	//Properties
	this.pokemonName = name;
	this.pokemonLevel = level;
	this.pokemonHealth = 2 * level;
	this.pokemonAttack = level;


// Method
		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
			
			let hpAftertackle = targetPokemon.pokemonHealth - this.pokemonAttack;
			
			console.log(targetPokemon.pokemonName + " health is now reduced to " + hpAftertackle);


			if (hpAftertackle <= 0){
				targetPokemon.faint()
			}


		this.faint = function(){
			console.log(this.pokemonName + " fainted!")
		}
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu)
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);